function getHash(){
    var hash = window.location.hash;
    var noHash=hash.substr(1);
    if(hash){
      if (noHash=="all"){
        return "all"
      }
      else{
        return '.'+noHash;
      }
    }
    else{
      return 'all';
    }
}
var loadHash=getHash();
$(function(){
  $('#post-listing').mixItUp({
        layout: {display:"block"},
        load:{
          filter: loadHash
        },
        callbacks: {
      onMixEnd:function(){
        var active=$(".filter.active");
        if (active.data("filter")=="all"){var theHash="all"}
        else{theHash=$(".filter.active").data("filter").substr(1);}
        window.location.hash=theHash;
      }
    }
    });
    var activeFilter=$(".filter.active");
    if(activeFilter.parent().hasClass("sub-filter-list")){
        $("#spacer").show();
        activeFilter.parent(".sub-filter-list").show();
        activeFilter.parent().parent().addClass("active-category");
    }
});

//Hide elements that are empty on team page
if ($("body").hasClass('team-member')){
        if ($.trim($(".sector-focus-industry").text())==""){
            $(".sector-focus").hide();
        }
    var stuff=$(".team-side-director").clone(); stuff.find("h2").empty(); 
    if ($.trim(stuff.text())==""){
            $(".team-side-director").hide();
        }
    if ($(".team-side-content:visible").length==0){
        $(".bio-side-bar").hide();
        $(".full-bio").removeClass("span8").addClass("span12").addClass("bio-full-width");
    }
    if ($.trim($(".hero-quote").text())==""){
        $(".team-quote-wrap").hide();
    }
    if ($.trim($(".pov-text").text())==""){
        $(".pov-strip").hide();
    }
    if($.trim($(".full-bio").text())==""){
        $(".team-bio-strip").hide();
    }
}


/*--

*/
/*--

*/


$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});