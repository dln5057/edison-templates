

// GLOBALS BAD
function fullheight() {
    var newHeight = $("html").height() + "px";
    var $fullScreenWrap = $(".fullscreen-wrapper");
    
    if ($fullScreenWrap.length > 0) {
        $fullScreenWrap.css("height", newHeight);
        $("svg", $fullScreenWrap).css("minHeight", newHeight);
        $("svg mask rect", $fullScreenWrap).css("minHeight", newHeight);
        $(".member-img-fit").css("minHeight", newHeight);
    }
}


$(function(){
    
    var fullHeightCalled = false;
    var $fullScreenWrap = $(".fullscreen-wrapper");
    
    var myFullHeight = function () {
        var newHeight = $("html").height() + "px";

        
        if (!fullHeightCalled) {
            fullHeightCalled = true;
            window.dispatchEvent(new Event('resize'));
            if ($fullScreenWrap.length > 0) {
                $fullScreenWrap.css("height", newHeight);
                $("svg", $fullScreenWrap).css("minHeight", newHeight);
                $("svg mask rect", $fullScreenWrap).css("minHeight", newHeight);
                $(".member-img-fit").css("minHeight", newHeight);
            }
        }
    }
    
    var wow = new WOW();
    wow.config.callback = myFullHeight;
    
    wow.init();
    
    // without this IE and Edge are jittery on the home page, consider wrapping into HP only function.
    if(navigator.userAgent.indexOf('Trident') != -1 || navigator.userAgent.indexOf('Edge') != -1) { // if IE
        $('body').on("mousewheel", function () {
            // remove default behavior
            event.preventDefault(); 

            //scroll without smoothing
            var wheelDelta = event.wheelDelta;
            var currentScrollPosition = window.pageYOffset;
            window.scrollTo(0, currentScrollPosition - wheelDelta);
        });
    }
    
    
    
    $('#hs_cos_wrapper_home_flex_1').show();
    $('.preload').hide();
    
    
    
    //new WOW().init();
    
    (function() {

      window.addEventListener("resize", resizeThrottler, false);
    
      var resizeTimeout;
      function resizeThrottler() {
        // ignore resize events as long as an actualResizeHandler execution is in the queue
        if ( !resizeTimeout ) {
          resizeTimeout = setTimeout(function() {
            resizeTimeout = null;
            fullheight();
         
           // The actualResizeHandler will execute at a rate of 15fps
           }, 66);
        }
      }
    }());

});

$(function(){
    fullheight();
});



//style the menu
$(function(){
    var $menu = $('#hs_menu_wrapper_main_menu');
    var $logo = $('.switch-logo-container img');
    var $navBarDef = $('.navbar-default');
    
    
    var last_known_scroll_position = 0;
    var ticking = false;
    
    function doSomething(scroll_pos) {
      if (scroll_pos > 50){
            $navBarDef.addClass("sticky");
            $menu.addClass("sticky");
        }
        else{
            $navBarDef.removeClass("sticky");
            $menu.removeClass("sticky");
        }
    }
    
    window.addEventListener('scroll', function(e) {
      last_known_scroll_position = window.scrollY;
      if (!ticking) {
        window.requestAnimationFrame(function() {
          doSomething(last_known_scroll_position);
          ticking = false;
        });
      }
      ticking = true;
    });
    
    
    
    $menu.find('li.hs-menu-depth-1').each(function(){
        if ($(this).find('a').text() == '-') {
            $(this).addClass('home-link');
            $(this).find('a').html($logo);
        }
    });
    $('#hs_menu_wrapper_mobile_menu').find('li.hs-menu-depth-1').each(function(){
        if ($(this).find('a').text() == '-') {
            $(this).remove();
        }
    });
    
  
        
});

//isotope
$(function(){
    if ($('.isotope-container').length != 0) {
        var $grid = $('.isotope-container').isotope({
            itemSelector: '.isotope',
        });
    
        $grid.imagesLoaded().progress( function() {
          $grid.isotope('layout');
        });
    }
});
$(function(){
    if ($('.team-iso').length != 0) {
        var filtersElem = document.querySelector('.team-filters');
        
        var $filters = $('li', $(filtersElem));
        
        var $subMenuToggle = $('.sub-menu-toggle');
        var $subMenu = $('.port-sub-filters-container');
        
        
        var setIsoLabelActive = function(activeFilter) {
            $filters.each(function(){
                var $this = $(this);
                if ($this.data('filter') !== activeFilter) {
                    $this.removeClass('active');
                } else {
                    $this.addClass('active');
                }
            });
        };
        
        $('body').on('click', '.sub-menu-toggle', function(event){
            var $target = $(event.target);
            
            if ($target.data('submenu')) {
                $subMenuToggle.not($target).removeClass('display');
                $subMenu.not($target.data('submenu')).removeClass('display');
                $target.toggleClass('display');
                $($target.data('submenu')).toggleClass('display');
            } else {
                $target.toggleClass('display').next('.port-sub-filters-container').toggleClass('display');
            }
        });
        
        var $team = new Isotope( '.team-iso', {
          itemSelector: '.isotope'
        });
    
        
        filtersElem.addEventListener( 'click', function( event ) {
            
            if ( !matchesSelector( event.target, 'li' ) || $(event.target).hasClass('industry-toggle-wrap')) {
                return;
            }
            var filterValue = event.target.getAttribute('data-filter');
            
            $team.arrange({ filter: filterValue });
            setIsoLabelActive(filterValue);
        });
        
       /* $('.team-filters ul li').on( 'click', function() {
            targetFilter = $(this).data('filter');
            $('.team-filters ul li').removeClass('active');
            $(this).addClass('active');
            $team.isotope({ filter: targetFilter });
        });*/
    }
});

$(function(){
    var $companyTeam = $('.company-team');
    if ($companyTeam.length > 0) {
        $companyTeam.each(function(){
            var swap = new TimelineLite({paused: true});
             swap.to($(this).find(".content-swap-wrapper .side-a"), .3, {top:-70, opacity:0},0)
                    .to($(this).find(".content-swap-wrapper .side-b"), .3, {top:-70, opacity:1},0)
            
            $(this).on('mouseenter', function(){
                swap.play();
                $(this).find('.img__a').hide();
                $(this).find('.img__b').show();
            });
            $(this).on('mouseleave', function(){
                swap.reverse();
                $(this).find('.img__b').hide();
                $(this).find('.img__a').show();
            });
        });
    }
});
$(function(){
    var $triColumn = $('.tri-column-container .col-md-4');
    if ($triColumn.length > 0) {
        $triColumn.each(function(){
            $(this).on('mouseenter', function(){
                $(this).find('.bg-overlay').fadeOut();
                $(this).find('.title-container').fadeOut();
                $(this).find('.body-content').hide();
                $(this).find('.link-container').fadeOut();
                $(this).find('.hover-img').fadeIn();
            });
            $(this).on('mouseleave', function(){
                $(this).find('.bg-overlay').fadeIn();
                $(this).find('.title-container').fadeIn();
                $(this).find('.body-content').fadeIn();
                $(this).find('.link-container').fadeIn();
                $(this).find('.hover-img').hide();
            });
        });
    }
});
$(function() {
if ($('.blog-grid').length != 0) {
  $('.blog-grid').isotope({
    itemSelector: '.grid-item',
    masonry: {
      columnWidth: 306,
      gutter: 5,
      fitWidth: true
    }
  });
}
})
$(function(){
    var $careerItem = $('.career-item');
    
    if ($careerItem.length > 0) {
        $('.contents', $careerItem).slideUp();
        $careerItem.each(function(){
            $(this).find('.career-titles').on('click', function(){
                if ($(this).find('h3').hasClass('active')) {
                    $(this).closest('.career-item').find('.contents').slideUp();
                    $(this).find('h3').removeClass('active');
                }else{
                    $(this).closest('.career-item').find('.contents').slideDown();
                    $(this).find('h3').addClass('active');
                }
            });
        });
    }
});


$(function(){
    if ($('.selectpicker').length != 0) {
        $('.selectpicker').selectpicker({
          style: 'btn-info',
          size: 4
        });
    }
});































