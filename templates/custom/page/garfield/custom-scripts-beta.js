$(function(){
    
    if ($('.port-iso').length > 0) {
    
        var filtersElem = document.querySelector('.main-port-filters');
        var $filters = $('li', $(filtersElem));
        var setIsoLabelActive = function(activeFilter) {
            $filters.each(function(){
                var $this = $(this);
                if ($this.data('filter') !== activeFilter) {
                    $this.removeClass('active');
                } else {
                    $this.addClass('active');
                }
            });
        };
        
        $('body').on('click', '.industry-toggle', function(event){
            $(event.target).toggleClass('display').next('.port-sub-filters-container').toggleClass('display');
        });
        
        var iso = new Isotope( '.port-iso', {
          itemSelector: '.isotope',
            masonry: {}
        });
        
        filtersElem.addEventListener( 'click', function( event ) {
            // only work with buttons
            if ( !matchesSelector( event.target, 'li' ) || $(event.target).hasClass('industry-toggle-wrap')) {
                return;
            }
            var filterValue = event.target.getAttribute('data-filter');
            
            iso.arrange({ filter: filterValue });
            setIsoLabelActive(filterValue);
        });
    }
});
$(function() {
    var $contactForm = $('#contact-us-form');
    var hash = window.location.hash;
    if ($contactForm.length > 0) {
        $('body').on('click', '.toggle-contact-form', function(event){
            $('body').toggleClass('form-displayed');
            $('#contact-us-form').toggleClass('display');
        });
        
        if (hash.indexOf('formOpen') != -1) {
            $('body').toggleClass('form-displayed');
            $('#contact-us-form').toggleClass('display');
        }
    
    }
  
});

/**** BLOG PAGE ****/

$(function(){
    
    
    var blogFunctions = function(){
        var topicsCount, $blogTopicsWrapper, currentUrl;
        
        var activeSet = false;
        
        
        function _topicListFixWidth () {
           topicsCount = ($('.topics-section', $blogTopicsWrapper).length) * 160;
           $('.topics-section-scroller', $blogTopicsWrapper).width(topicsCount);
        }
        
        function _setEvents () {
            $('body').on('click', '.toggle-topics-display', function(event){
                $blogTopicsWrapper.toggleClass('display');
            });
        }
        
        function _setActiveTopic () {
            $('a', $blogTopicsWrapper).each(function(){
                if (currentUrl.indexOf($(this).attr('href')) != -1 && !activeSet) {
                    activeSet = true;
                    $(this).addClass('active');
                }
            });
        }
        
        function _init () {
            currentUrl = window.location.href;
            $blogTopicsWrapper = $('#blog-topics');
            _topicListFixWidth();
            _setEvents();
            _setActiveTopic();
        }
        
        return {
         init: _init
        }
    }();
    
    if ($('body').hasClass('hs-blog-listing')) {
        blogFunctions.init();
    }
    
});

$(function(){
    if ($('.isotope-container').length != 0) {
        var $grid = new Isotope('.isotope-container', {
            itemSelector: '.isotope',
            fitWidth: true
        });
    
        $grid.imagesLoaded().progress( function() {
          $grid.isotope('layout');
        });
    }
});

$(function() {
if ($('.blog-grid').length != 0) {
  var $blogIso = new Isotope('.blog-grid', {
    itemSelector: '.grid-item',
    masonry: {
      columnWidth: 306,
      gutter: 5
    }
  });
}
});